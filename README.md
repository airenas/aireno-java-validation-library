# Simple Java fluent validator library #

It allows quickly validate any variable in one line, without if clauses. It throws exception if validation fails. If needed then a custom exception type could be plugged in.

---
###Java code build notes

1. Build, test and install the library using maven
```bash
    mvn install
```

2. Usage

- Include the lib in pom.xml:
```xml
        <dependency>
            <groupId>com.aireno</groupId>
            <artifactId>avalid-default</artifactId>
            <version>1.1-SNAPSHOT</version>
        </dependency>
```
- Use in code
```java
    String paramemer = ...
    
    AValid.validate().that(parameter).as("Parameter is not as expected").isEqualTo("aaa");
    
    // parameter is validated and it has value "aaa".
    // Or the exception was thrown with message "Parameter is not as expected"
    System.out.println(parameter);

```

---
### Author

**Airenas Vaičiūnas**

* [bitbucket.org/airenas](https://bitbucket.org/airenas)
* [linkedin.com/in/airenas](https://www.linkedin.com/in/airenas/)


---
### License

Copyright © 2019, [Airenas Vaičiūnas](https://bitbucket.org/airenas).
Released under the [The 3-Clause BSD License](LICENSE).

---