package com.aireno.avalid;

import org.junit.Test;

/**
 * @author Airenas Vaiciunas.
 */
public class AValidTest {

    @Test(expected = NotValidException.class)
    public void isNotNull_Fails() {
        AValid.validate().that((String) null).isNotNull();
    }

    @Test(expected = NotValidException.class)
    public void isEquals_Fails() {
        AValid.validate().that("aaa").as("Wong parameter %s").isEqualTo("aa");
    }

    @Test()
    public void isEquals_OK() {
        AValid.validate().that("aaa").as("Param test").isEqualTo("aaa");
        AValid.validate().that("aaa").as(e -> e.noMessage()).as("OLIA TEST").isEqualTo("aaa");
    }
}
