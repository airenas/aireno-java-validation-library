/*
Copyright (c) 2017, Airenas Vaičiūnas (airenass@gmail.com)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.aireno.avalid.fluent;

import com.aireno.avalid.failure.ExceptionConfigurator;
import com.aireno.avalid.failure.ExceptionProvider;
import com.aireno.avalid.fluent.base.FluentValidatorBase;
import com.aireno.avalid.fluent.base.FluentValidatorGeneric;

import java.util.function.Consumer;

/**
 * @author Airenas Vaičiūnas
 * @since 1/29/16
 */
public class ArrayValidator<T, TException extends RuntimeException, TExceptionConfigurator extends ExceptionConfigurator>
        extends FluentValidatorGeneric<T[], TException, TExceptionConfigurator> {
    public ArrayValidator(ExceptionProvider<TException, TExceptionConfigurator> exceptionProvider, T[] item) {
        super(exceptionProvider, item);
    }

    public ArrayValidator<T, TException, TExceptionConfigurator> isEmpty() {
        return me(isTrue(item.length == 0, "Array is not empty"));
    }

    public ArrayValidator<T, TException, TExceptionConfigurator> isNotEmpty() {
        return me(isTrue(item.length > 0, "Array is empty"));
    }

    @Override
    public ArrayValidator<T, TException, TExceptionConfigurator> isNotNull() {
        return me(super.isNotNull());
    }

    @Override
    public ArrayValidator<T, TException, TExceptionConfigurator> as(String message, Object... params) {
        return me(super.as(message, params));
    }

    @Override
    public ArrayValidator<T, TException, TExceptionConfigurator> as(Consumer<TExceptionConfigurator> configurator) {
        return me(super.as(configurator));
    }

    private ArrayValidator<T, TException, TExceptionConfigurator> me(FluentValidatorBase<TException, TExceptionConfigurator> item) {
        return (ArrayValidator<T, TException, TExceptionConfigurator>) item;
    }
}
