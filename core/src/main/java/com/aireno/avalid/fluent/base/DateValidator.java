/*
Copyright (c) 2016, Airenas Vaičiūnas (airenass@gmail.com)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.aireno.avalid.fluent.base;

import com.aireno.avalid.failure.ExceptionConfigurator;
import com.aireno.avalid.failure.ExceptionProvider;

import java.time.Instant;
import java.util.function.Consumer;

/**
 * @author Airenas Vaičiūnas
 * @since 1/29/16
 */
public class DateValidator<TException extends RuntimeException, TExceptionConfigurator extends ExceptionConfigurator>
        extends FluentValidatorGeneric<Long, TException, TExceptionConfigurator> {
    public DateValidator(ExceptionProvider<TException, TExceptionConfigurator> exceptionProvider, Long item) {
        super(exceptionProvider, item);
    }

    public DateValidator<TException, TExceptionConfigurator> isEqualTo(long value) {
        return me(isTrue(item == value, "Value is %d, expected: %d", item, value));
    }

    public com.aireno.avalid.fluent.DateValidator<TException, TExceptionConfigurator> asDate() {
        isNotNull();
        return new com.aireno.avalid.fluent.DateValidator<>(exceptionProvider, Instant.ofEpochMilli(item));
    }

    public DateValidator<TException, TExceptionConfigurator> isGreaterThan(long value) {
        return me(isTrue(item > value, "Value %d is not greater than %d", item, value));
    }

    public DateValidator<TException, TExceptionConfigurator> isLessThan(long value) {
        return me(isTrue(item < value, "Value %d is not less than %d", item, value));
    }

    @Override
    public DateValidator<TException, TExceptionConfigurator> isNotNull() {
        return me(super.isNotNull());
    }

    @Override
    public DateValidator<TException, TExceptionConfigurator> isNotNull(Consumer<TExceptionConfigurator> builder) {
        return me(super.isNotNull(builder));
    }

    @Override
    public DateValidator<TException, TExceptionConfigurator> as(String message, Object... params) {
        return me(super.as(message, params));
    }

    @Override
    public DateValidator<TException, TExceptionConfigurator> as(Consumer<TExceptionConfigurator> configurator) {
        return me(super.as(configurator));
    }

    private DateValidator<TException, TExceptionConfigurator> me(FluentValidatorBase<TException, TExceptionConfigurator> item) {
        return (DateValidator<TException, TExceptionConfigurator>) item;
    }
}
