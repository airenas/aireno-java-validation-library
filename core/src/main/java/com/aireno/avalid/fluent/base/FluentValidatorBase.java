/*
Copyright (c) 2016, Airenas Vaičiūnas (airenass@gmail.com)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.aireno.avalid.fluent.base;

import com.aireno.avalid.failure.ExceptionConfigurator;
import com.aireno.avalid.failure.ExceptionProvider;

import java.util.function.Consumer;

/**
 * @author Airenas Vaičiūnas
 * @since 1/29/16
 */
public abstract class FluentValidatorBase<TException extends RuntimeException, TExceptionConfigurator extends ExceptionConfigurator> {
    protected final ExceptionProvider<TException, TExceptionConfigurator> exceptionProvider;

    public FluentValidatorBase(ExceptionProvider<TException, TExceptionConfigurator> exceptionProvider) {
        this.exceptionProvider = exceptionProvider;
    }

    protected void fail(Object ...params) {
        throw exceptionProvider.create(params);
    }

    protected void fail(Consumer<TExceptionConfigurator> configurator, Object ...params) {
        configurator.accept(exceptionProvider.getExceptionConfigurator());
        fail(params);
    }

    public FluentValidatorBase<TException, TExceptionConfigurator> as(String validationName, Object... params) {
        exceptionProvider.getExceptionConfigurator().withName(validationName, params);
        return this;
    }

    public FluentValidatorBase<TException, TExceptionConfigurator> as(Consumer<TExceptionConfigurator> configurator) {
        configurator.accept(exceptionProvider.getExceptionConfigurator());
        return this;
    }

    protected FluentValidatorBase<TException, TExceptionConfigurator> isTrue(boolean value) {
        return isTrue(value, "Condition is not valid");
    }

    protected FluentValidatorBase<TException, TExceptionConfigurator> isTrue(boolean value, String message, Object ...params) {
        return isTrue(value, c -> c.setMessage(message), params);
    }

    protected FluentValidatorBase<TException, TExceptionConfigurator> isTrue(boolean value, Consumer<TExceptionConfigurator> configurator, Object ...params) {
        if (!value) {
            fail(configurator, params);
        }
        return this;
    }

    protected FluentValidatorBase<TException, TExceptionConfigurator> isFalse(boolean value) {
        return isTrue(!value, "Condition expected to be false");
    }
}
