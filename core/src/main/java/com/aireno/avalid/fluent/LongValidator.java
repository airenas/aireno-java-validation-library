/*
Copyright (c) 2016, Airenas Vaičiūnas (airenass@gmail.com)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.aireno.avalid.fluent;

import com.aireno.avalid.failure.ExceptionConfigurator;
import com.aireno.avalid.failure.ExceptionProvider;
import com.aireno.avalid.fluent.base.FluentValidatorBase;
import com.aireno.avalid.fluent.base.FluentValidatorGeneric;

import java.time.Instant;
import java.util.function.Consumer;

/**
 * @author Airenas Vaičiūnas
 * @since 1/29/16
 */
public class LongValidator<TException extends RuntimeException, TExceptionConfigurator extends ExceptionConfigurator>
        extends FluentValidatorGeneric<Long, TException, TExceptionConfigurator> {
    public LongValidator(ExceptionProvider<TException, TExceptionConfigurator> exceptionProvider, Long item) {
        super(exceptionProvider, item);
    }

    public LongValidator<TException, TExceptionConfigurator> isEqualTo(long value) {
        isNotNull();
        return me(isTrue(item == value, "Value is %d, expected: %d", item, value));
    }

    public DateValidator<TException, TExceptionConfigurator> asDate() {
        isNotNull();
        return new DateValidator<>(exceptionProvider, Instant.ofEpochMilli(item));
    }

    public LongValidator<TException, TExceptionConfigurator> isGreaterThan(long value) {
        isNotNull();
        return me(isTrue(item > value, "Value %d is not greater than %d", item, value));
    }

    public LongValidator<TException, TExceptionConfigurator> isLessThan(long value) {
        isNotNull();
        return me(isTrue(item < value, "Value %d is not less than %d", item, value));
    }

    public LongValidator<TException, TExceptionConfigurator> isGreaterEqual(long value) {
        isNotNull();
        return me(isTrue(item >= value, "Value %d is not greater or equal than %d", item, value));
    }

    public LongValidator<TException, TExceptionConfigurator> isLessEqual(long value) {
        isNotNull();
        return me(isTrue(item <= value, "Value %d is not less or equal than %d", item, value));
    }

    @Override
    public LongValidator<TException, TExceptionConfigurator> isNotNull() {
        return me(super.isNotNull());
    }

    @Override
    public LongValidator<TException, TExceptionConfigurator> isNotNull(Consumer<TExceptionConfigurator> builder) {
        return me(super.isNotNull(builder));
    }

    @Override
    public LongValidator<TException, TExceptionConfigurator> as(String message, Object... params) {
        return me(super.as(message, params));
    }

    @Override
    public LongValidator<TException, TExceptionConfigurator> as(Consumer<TExceptionConfigurator> configurator) {
        return me(super.as(configurator));
    }

    private LongValidator<TException, TExceptionConfigurator> me(FluentValidatorBase<TException, TExceptionConfigurator> item) {
        return (LongValidator<TException, TExceptionConfigurator>) item;
    }
}
