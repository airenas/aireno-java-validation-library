/*
Copyright (c) 2016, Airenas Vaičiūnas (airenass@gmail.com)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.aireno.avalid.failure;

/**
 * @author Airenas Vaičiūnas
 * @since 1/29/16
 */
public class ExceptionConfiguratorBase implements ExceptionConfigurator {
    protected String validationName;
    protected boolean isIncludeMessage = true;
    protected String message;
    protected Throwable cause;

    @Override
    public ExceptionConfiguratorBase withName(String validationName, Object... args) {
        this.validationName = format(validationName, args);
        return this;
    }

    public ExceptionConfiguratorBase withMessage(String message, Object... args) {
        this.message = format(message, args);
        isIncludeMessage = true;
        return this;
    }

    @Override
    public ExceptionConfiguratorBase noMessage() {
        isIncludeMessage = false;
        return this;
    }

    public ExceptionConfiguratorBase withCause(Throwable cause) {
        this.cause = cause;
        return this;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String getName() {
        return validationName;
    }

    @Override
    public boolean isIncludeMessage() {
        return isIncludeMessage;
    }

    @Override
    public Throwable getCause() {
        return cause;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    private String format(String s, Object... object) {
        return object.length > 0 ? String.format(s, object) : s;
    }
}
