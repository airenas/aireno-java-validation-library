/*
Copyright (c) 2016, Airenas Vaičiūnas (airenass@gmail.com)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
package com.aireno.avalid;

import com.aireno.avalid.failure.ExceptionConfigurator;
import com.aireno.avalid.failure.ExceptionProvider;
import com.aireno.avalid.fluent.*;
import com.aireno.avalid.fluent.base.FluentValidatorGeneric;
import org.jetbrains.annotations.Nullable;

import java.time.Instant;
import java.util.Collection;
import java.util.Date;
import java.util.function.Consumer;

/**
 * Main validation class
 *
 * @author Airenas Vaičiūnas
 * @since 1/29/16
 */
public abstract class Validator<TException extends RuntimeException, TExceptionConfigurator extends ExceptionConfigurator> {
    final ExceptionProvider<TException, TExceptionConfigurator> exceptionProvider;

    public Validator(ExceptionProvider<TException, TExceptionConfigurator> exceptionProvider) {
        this.exceptionProvider = exceptionProvider;
    }

    public StringValidator<TException, TExceptionConfigurator> that(String value) {
        return new StringValidator<>(exceptionProvider, value);
    }

    public IntegerValidator<TException, TExceptionConfigurator> that(int value) {
        return new IntegerValidator<>(exceptionProvider, value);
    }

    public LongValidator<TException, TExceptionConfigurator> that(long value) {
        return new LongValidator<>(exceptionProvider, value);
    }

    public IntegerValidator<TException, TExceptionConfigurator> that(Integer value) {
        return new IntegerValidator<>(exceptionProvider, value);
    }

    public LongValidator<TException, TExceptionConfigurator> that(Long value) {
        return new LongValidator<>(exceptionProvider, value);
    }

    public DateValidator<TException, TExceptionConfigurator> that(Instant value) {
        return new DateValidator<>(exceptionProvider, value);
    }

    public DateValidator<TException, TExceptionConfigurator> that(Date value) {
        return new DateValidator<>(exceptionProvider, Instant.ofEpochMilli(value.getTime()));
    }

    public BooleanValidator<TException, TExceptionConfigurator> that(boolean value) {
        return new BooleanValidator<>(exceptionProvider, value);
    }

    public <T> CollectionValidator<T, TException, TExceptionConfigurator> that(Collection<T> collection) {
        return new CollectionValidator<>(exceptionProvider, collection);
    }

    public <T> ArrayValidator<T, TException, TExceptionConfigurator> that(T[] array) {
        return new ArrayValidator<T, TException, TExceptionConfigurator>(exceptionProvider, array);
    }

    public <T> FluentValidatorGeneric<T, TException, TExceptionConfigurator> that(@Nullable T object) {
        return new FluentValidatorGeneric<>(exceptionProvider, object);
    }

    public TException newException(Throwable t, String message, Object ... params) {
        return newException((e) -> e.withCause(t).withName(message).noMessage(), params);
    }

    public TException newException(String message, Object ... params) {
        return newException((c) -> c.withName(message).noMessage(), params);
    }

    public TException newException(Consumer<TExceptionConfigurator> configurator, Object ... params) {
        configurator.accept(exceptionProvider.getExceptionConfigurator());
        return exceptionProvider.create(params);
    }
}

