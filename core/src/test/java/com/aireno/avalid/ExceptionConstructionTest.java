package com.aireno.avalid;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.function.Consumer;


/**
 * @author Airenas Vaiciunas.
 */
@SuppressWarnings("ThrowableResultOfMethodCallIgnored")
public class ExceptionConstructionTest {
    TestValidator validator;

    @Before
    public void init() {
        validator = new TestValidator(new TestExceptionProvider());
    }

    @Test
    public void defaultMessage() {
        TestException exception = getException((v) -> v.that(false).isTrue());
        Assert.assertEquals("Validation failed", exception.getMessage());
    }

    @Test
    public void customMessage() {
        TestException exception = getException((v) -> v.that(false).as("Olia").isTrue());
        Assert.assertEquals("Olia", exception.getMessage());
    }

    @Test
    public void customMessageWithParam() {
        TestException exception = getException((v) -> v.that(false).as("Olia %s", "haha").isTrue());
        Assert.assertEquals("Olia haha", exception.getMessage());
    }

    @Test
    public void messageWithDetails() {
        TestException exception = getException((v) -> v.that("aa").as("Olia %s").isEqualTo("ss"));
        Assert.assertEquals("[Olia aa] Value is aa, expected: ss", exception.getMessage());
    }

    @Test
    public void messageNoDetails() {
        TestException exception = getException((v) -> v.that("aa").as(b -> b.withName("Olia %s").noMessage()).isEqualTo("ss"));
        Assert.assertEquals("Olia aa", exception.getMessage());
    }

    @Test
    public void customMessageWithItemAsParam() {
        TestException exception = getException((v) -> v.that(false).as("Olia %s").isTrue());
        Assert.assertEquals("Olia false", exception.getMessage());
    }

    private TestException getException(Consumer<TestValidator> validatorConsumer) {
        try {
            validatorConsumer.accept(validator);
        } catch (TestException e) {
            return e;
        }
        Assert.fail("Expected error");
        return new TestException("");
    }
}
