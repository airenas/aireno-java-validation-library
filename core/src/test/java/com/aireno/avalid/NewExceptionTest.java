package com.aireno.avalid;

import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;


/**
 * @author Airenas Vaiciunas.
 */
@SuppressWarnings("ThrowableResultOfMethodCallIgnored")
public class NewExceptionTest {
    TestValidator validator;

    @Before
    public void init() {
        validator = new TestValidator(new TestExceptionProvider());
    }

    @Test
    public void message() {
        TestException exception = validator.newException("Olia");
        Assert.assertEquals("Olia", exception.getMessage());
    }

    @Test
    public void messageWithParams() {
        TestException exception = validator.newException("Olia %s", "aa");
        Assert.assertEquals("Olia aa", exception.getMessage());
    }

    @Test
    public void messageWithParamsAndCause() {
        RuntimeException cause = new RuntimeException();
        TestException exception = validator.newException(cause, "Olia %s", "aa");
        Assert.assertEquals("Olia aa", exception.getMessage());
        Assert.assertEquals(cause, exception.getCause());
    }
}
