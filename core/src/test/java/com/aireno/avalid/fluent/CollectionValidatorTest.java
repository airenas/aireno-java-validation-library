package com.aireno.avalid.fluent;

import com.aireno.avalid.TestException;
import com.aireno.avalid.TestExceptionProvider;
import com.aireno.avalid.TestValidator;
import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.Collections;

/**
 * @author Airenas Vaiciunas.
 */
public class CollectionValidatorTest {
    TestValidator validator;

    @Before
    public void init() {
        validator = new TestValidator(new TestExceptionProvider());
    }


    @Test(expected = TestException.class)
    public void isNotNull_Fails() {
        validator.that((Collection<String>) null).isNotNull();
    }

    @Test
    public void isNotNull_OK() {
        validator.that(Collections.emptyList()).isNotNull();
    }

    @Test(expected = TestException.class)
    public void isEmpty_Fails() {
        validator.that(Collections.singleton("aa")).isEmpty();
    }

    @Test
    public void isEmpty_OK() {
        validator.that(Collections.emptyList()).isEmpty();
    }

    @Test
    public void isNotEmpty_OK() {
        validator.that(Collections.singleton("aa")).isNotEmpty();
    }

    @Test(expected = TestException.class)
    public void isNotEmpty_Fails() {
        validator.that(Collections.emptyList()).isNotEmpty();
    }
}
