package com.aireno.avalid.fluent;

import com.aireno.avalid.TestException;
import com.aireno.avalid.TestExceptionProvider;
import com.aireno.avalid.TestValidator;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Airenas Vaiciunas.
 */
public class IntegerValidatorTest {
    TestValidator validator;

    @Before
    public void init() {
        validator = new TestValidator(new TestExceptionProvider());
    }


    @Test(expected = TestException.class)
    public void isTrue_Fails() {
        validator.that(false).isTrue();
    }

    @Test
    public void isTrue_OK() {
        validator.that(true).isTrue();
    }

    @Test
    public void isFalse_OK() {
        validator.that(false).isFalse();
    }

    @Test(expected = TestException.class)
    public void isFalse_Fails() {
        validator.that(true).isFalse();
    }

    @Test(expected = TestException.class)
    public void asNullObject_OK() {
        validator.that((Integer) null).isLessThan(11);
    }

    @Test(expected = TestException.class)
    public void isGreaterEqual_Fails() {
        validator.that(10).isGreaterEqual(11);
    }

    @Test
    public void isGreaterEqual_OK() {
        validator.that(10).isGreaterEqual(9);
    }

    @Test
    public void isGreaterEqual_Equal_OK() {
        validator.that(10).isGreaterEqual(10);
    }

    @Test(expected = TestException.class)
    public void isLessEqual_Fails() {
        validator.that(10).isLessThan(9);
    }

    @Test
    public void isLessEqual_Equal_OK() {
        validator.that(10).isLessEqual(10);
    }
    @Test
    public void isLessEqual_OK() {
        validator.that(10).isLessEqual(12);
    }
}
