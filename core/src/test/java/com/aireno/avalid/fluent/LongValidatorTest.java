package com.aireno.avalid.fluent;

import com.aireno.avalid.TestException;
import com.aireno.avalid.TestExceptionProvider;
import com.aireno.avalid.TestValidator;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Airenas Vaiciunas.
 * @since 3/18/16
 */
public class LongValidatorTest {
    TestValidator validator;

    @Before
    public void init() {
        validator = new TestValidator(new TestExceptionProvider());
    }


    @Test(expected = TestException.class)
    public void isEquals_Fails() {
        validator.that(0L).isEqualTo(1);
    }

    @Test
    public void isEquals_OK() {
        validator.that(0L).isEqualTo(0);
    }

    @Test(expected = TestException.class)
    public void isGreater_Fails() {
        validator.that(0L).isGreaterThan(1);
    }

    @Test
    public void isGreater_OK() {
        validator.that(10L).isGreaterThan(0);
    }

    @Test(expected = TestException.class)
    public void isLess_Fails() {
        validator.that(0L).isLessThan(-1);
    }

    @Test
    public void isLess_OK() {
        validator.that(10L).isLessThan(11);
    }

    @Test
    public void asObject_OK() {
        validator.that(new Long(10)).isLessThan(11);
    }

    @Test(expected = TestException.class)
    public void asNullObject_OK() {
        validator.that((Long) null).isLessThan(11);
    }

    @Test(expected = TestException.class)
    public void isGreaterEqual_Fails() {
        validator.that(10L).isGreaterEqual(11);
    }

    @Test
    public void isGreaterEqual_OK() {
        validator.that(10L).isGreaterEqual(9);
    }

    @Test
    public void isGreaterEqual_Equal_OK() {
        validator.that(10L).isGreaterEqual(10);
    }

    @Test(expected = TestException.class)
    public void isLessEqual_Fails() {
        validator.that(10L).isLessThan(9);
    }

    @Test
    public void isLessEqual_Equal_OK() {
        validator.that(10L).isLessEqual(10);
    }
    @Test
    public void isLessEqual_OK() {
        validator.that(10L).isLessEqual(12);
    }
}
