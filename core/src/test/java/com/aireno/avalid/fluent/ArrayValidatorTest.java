package com.aireno.avalid.fluent;

import com.aireno.avalid.TestException;
import com.aireno.avalid.TestExceptionProvider;
import com.aireno.avalid.TestValidator;
import org.junit.Before;
import org.junit.Test;

/**
 * Test Array validator
 *
 * @author Airenas Vaiciunas
 * @since 3/31/17
 */
public class ArrayValidatorTest {
    TestValidator validator;

    @Before
    public void init() {
        validator = new TestValidator(new TestExceptionProvider());
    }


    @Test(expected = TestException.class)
    public void isNotNull_Fails() {
        validator.that((String[]) null).isNotNull();
    }

    @Test
    public void isNotNull_OK() {
        validator.that(new String[0]).isNotNull();
    }

    @Test(expected = TestException.class)
    public void isEmpty_Fails() {
        validator.that(new String[]{"a"}).isEmpty();
    }

    @Test
    public void isEmpty_OK() {
        validator.that(new String[]{}).isEmpty();
    }

    @Test
    public void isNotEmpty_OK() {
        validator.that(new String[]{"a"}).isNotEmpty();
    }

    @Test(expected = TestException.class)
    public void isNotEmpty_Fails() {
        validator.that(new String[]{}).isNotEmpty();
    }
}
