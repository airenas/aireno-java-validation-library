package com.aireno.avalid.fluent;

import com.aireno.avalid.TestException;
import com.aireno.avalid.TestExceptionProvider;
import com.aireno.avalid.TestValidator;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Airenas Vaiciunas.
 */
public class BooleanValidatorTest {
    TestValidator validator;

    @Before
    public void init() {
        validator = new TestValidator(new TestExceptionProvider());
    }


    @Test(expected = TestException.class)
    public void isTrue_Fails() {
        validator.that(false).isTrue();
    }

    @Test
    public void isTrue_OK() {
        validator.that(true).isTrue();
    }

    @Test
    public void isFalse_OK() {
        validator.that(false).isFalse();
    }

    @Test(expected = TestException.class)
    public void isFalse_Fails() {
        validator.that(true).isFalse();
    }
}
