package com.aireno.avalid.fluent;

import com.aireno.avalid.TestException;
import com.aireno.avalid.TestExceptionProvider;
import com.aireno.avalid.TestValidator;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;

/**
 * @author Airenas Vaiciunas.
 * @since 3/18/16
 */
public class DateValidatorTest {
    TestValidator validator;

    @Before
    public void init() {
        validator = new TestValidator(new TestExceptionProvider());
    }


    @Test(expected = TestException.class)
    public void isEquals_Fails() {
        validator.that(Instant.ofEpochMilli(10)).isEqualTo(Instant.now());
    }

    @Test
    public void isEquals_OK() {
        validator.that(Instant.ofEpochMilli(10)).isEqualTo(Instant.ofEpochMilli(10));
    }

    @Test(expected = TestException.class)
    public void isBefore_Fails() {
        validator.that(Instant.now().plusSeconds(100)).isBefore(Instant.now());
    }

    @Test
    public void isBefore_OK() {
        validator.that(Instant.now().minusSeconds(100)).isBefore(Instant.now());
    }

    @Test(expected = TestException.class)
    public void isAfter_Fails() {
        validator.that(Instant.now().minusSeconds(100)).isAfter(Instant.now());
    }

    @Test
    public void isAfter_OK() {
        validator.that(Instant.now().plusSeconds(100)).isAfter(Instant.now());
    }
}
