package com.aireno.avalid.fluent;

import com.aireno.avalid.TestException;
import com.aireno.avalid.TestExceptionProvider;
import com.aireno.avalid.TestValidator;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Airenas Vaiciunas.
 */
public class StringValidatorTest {
    TestValidator validator;

    @Before
    public void init() {
        validator = new TestValidator(new TestExceptionProvider());
    }


    @Test(expected = TestException.class)

    public void isNotNull_Fails() {
        validator.that((String) null).isNotNull();
    }

    @Test
    public void isNotNull_OK() {
        validator.that("").isNotNull();
    }

    @Test
    public void isNotEmpty_OK() {
        validator.that("aa").isNotEmpty();
    }

    @Test
    public void isEmpty_OK() {
        validator.that("").isEmpty();
    }

    @Test(expected = TestException.class)
    public void isNotEmpty_Fails() {
        validator.that("").isNotEmpty();
    }

    @Test(expected = TestException.class)
    public void isEquals_Fails() {
        validator.that("aaa").isEqualTo("aa");
    }

    @Test()
    public void isEquals_OK() {
        validator.that("aaa").isEqualTo("aaa");
    }
}
